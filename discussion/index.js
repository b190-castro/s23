console.log("Hello World")
// SECTION - Objects
/*
	- objects are data types that are used to represent real world objects
	- collection of related data and/or functionalities
	- in Javascript, most features like strings and arrays are considered to be objects

	- their difference in Javascript objects is that the JS object has "key: value" pair

	- keys are also referred to as "properties" while the value is the figure on the right side of the colon

	- objects are commmonly initialized or declaredusing the let + objectName and this object value will start with curly brace ({}) that is also called "Object Literals"

	SYNTAX(using initializers):
		let/const onjectName = {
			keyA: valueA,
			keyB: valueB
		}
*/

let cellphone = {
	name: "Nokia 3210",
	manufacturedDate: 1999
}

console.log("Result from creating objects using initializers")
console.log(cellphone);
console.log(typeof cellphone);

/*
	Miniactivty
		create an object called car
			name/model:
			releaseDate:
			yearReleased:
*/

/*let car = {
	name: "Mitsubishi/Expander Cross",
	yearReleased: 2021
}

console.log(car)*/


// creating objects using constructor function
/*
	- creates a reusable function to create/construct several onjects that have same data structure
	- useful for creating multiple instances /copies of an object
	- instance is a concrete occurence of any object which emphasizes on any object which emphasizes on the distinct/unique identity of it.

	SYNTAX:
		function ObjectName(keyA,keyB){
			this.keyA = keyA;
			this.keyB = keyB
		}

*/
/*
	this - allows us to assign a new property for the object by associating them with values received from a constructor function's parameters
*/
function Laptop(name, manufacturedDate){
	this.name = name;
	this.manufacturedDate = manufacturedDate;
}
// this is a unique instance/copy of the Laptop object
/*
	new - creates an instance of an object

	- objects and instances are often interchanged because object literals and instances are distinct/unique
*/
// let laptop = Laptop("Dell",2012); - not using "new" keyword. would return undefined if we try to log the instance in the console since we don't have a return statement in the Laptop function
let laptop = new Laptop("Dell",2012);
console.log("Result from creating objects using initializers")
console.log(laptop);
console.log(typeof laptop);

// creates a new Laptop object
let laptop2 = new Laptop("Lenovo",2008);
console.log("Result from creating objects using initializers")
console.log(laptop2);
console.log(typeof laptop2);

// create empty objects
let computer = {};
let myComputer = new Object();
console.log("Result from creating objects using initializers")
console.log(myComputer);
console.log(typeof laptop2);

// Accessing objects
/*
	in arrays, we can access the elements inside through the use of square notation; arrayName[index]. In Javascript, it's a bit different since we have to use dot notation to access the property/key

	SYNTAX:
		key.name
*/

// using dot notation
console.log("Result from dot notation: " + laptop.name)

// using square bracket
// console.log("Result from square brackets: " + laptop[name]); - returns undefined since there is no element inside the laptop that can be accessed using square bracket notation

// array of objects
/*
	- accessing arrays would bbe the priority since that is the first data type that we have to access
	- since we have [ ] to access, we will use laptops[1] to access the laptop2 element inside the laptops array
	- since the laptop 2 is an object, access its properties would require us to use dot notation
	- meaning we have to use laptop2.name to get the value for the "name" property
		hence, we have this Syntax

	SYNTAX:
		arrayName[index].property
*/
let laptops = [ laptop,laptop2 ];
console.log(laptops[1].name)

// accessing laptops array - laptop - manufacturedDate
console.log(laptops[0].manufacturedDate);
// using string inside the 2nd square bracket would let us access the element. This may lead to confusions as to what data type is being accessed
console.log(laptops[0]["manufacturedDate"]);

// SECTION - initializing, adding, deleting, and reassigning object properties
/*
	- like any other variables in Javascript, objects may have their properties initialize or added after the object was created/declared
	- this is useful for times when an object's properties are undetermned at the time of creating them
*/
let car = [];
console.log(car);
// adding properties
car.name = "Honda Vios";
car.manufacturedDate = 2019;
console.log(car);
/*
	- while using sqquare brackets will give the same feature as using dot notation, it might lead us to create unconventional naming for the properties which might lead future confusions when we try to access them
*/
car["manufactured date"] = 2019;
console.log(car);

// deleting of properties
delete car["manufactured date"];
// delete car.manufactureDate; - can also be used with dot notation
console.log(car);

// reassigning of properties
/*
	change the name of the car into "Dodge Charger R/T"
*/
car.name = "Dodge Charger R/T";
console.log(car);


// Object Methods
/*
	- an object method is a function that is set by the dev to be the value of one of the properties
	- they are also functions and one of the differences they have us that methods are functions related to a specific object
	- these are useful for creating object-specific functions which are used to perform tasks on them
	- similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how they should do it.
*/
/*
	when we try to outsource a function to be stored inside a property, it would return an error since object methods have to be defined inside an object
*/
let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is " + this.name)
	}
}
console.log(person);
console.log("Result from object method");
person.talk();

// adding a method
	person.walk = function(){
		// using template literals backticks + $[] + string data type
		console.log(`${this.name} walked 25 steps`)
	}
person.walk();

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		state: "Texas"
	},
	emails:['joe@mail.com',"johnHandsome@mail.com"],
	introduce: function(){
		console.log("Hello! Ny name is " + this.firstName + " " + this.lastName)
	}
}
friend.introduce();
console.log(friend);

// "Hello! My name is <firstName> + <lastName>." - logged in the console when we call the introduce

// real-world application of objects
/*
	Scenario
		- we would like to create a game that would have several pokemon interact with each other
		- every pokemon would have the same set of stats, properties and functions
*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");	
	},
	faint: function(){
		console.log("Pokemon Fainted")
	}
};
console.log(myPokemon)

// using constructor function
function Pokemon (name,level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// Method
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	};
	this.faint = function(){
		console.log(this.name + " fainted.");
	}
}

//creating new pokemon
let pikachu = new Pokemon ("Pikachu", 16);
let rattata = new Pokemon ("Rattata", 8);

//using the tackle method from pikachu with rattata as its target
pikachu.tackle(rattata);