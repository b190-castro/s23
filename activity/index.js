let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon:["Pikachu","Lucario","Gengar","Dragonite","Sirfetch'd","Dracovish"],
	friends:{
		Kanto:["Misty","Brock"],
		Hoenn:["May","Max"],
		Unova:["Iris","Cilan"],
		Kalos:["Serena, Clemont, Bonnie"],
		Galar:["Goh, Chloe, Dawn"],
	},
	talk: function(){
		console.log("Pikachu! I choose you!")
	}

}
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer.pokemon);

console.log("Result of talk method:");
trainer.talk();


let pokemon1 = {
	name: "Bidoof",
	level: 100,
	health: 300,
	attack: 500,
	tackle: function(){
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to " + (health - attack))
	}
}
console.log(pokemon1);

let pokemon2 = {
	name: "Arceus",
	level: 90,
	health: 100,
	attack: 100,
	tackle: function(){
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to " + (health - attack))
	}
}
console.log(pokemon2);

let pokemon3 = {
	name: "Rowlet",
	level: 100,
	health: 500,
	attack: 700,
	tackle: function(){
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to " + (health - attack))
	}
}
console.log(pokemon3);

function Pokemon (name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = attack;

	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		if (target.health >= 0 ){
			console.log(target.name + " fainted.");
		this.faint();
		}
	}
	this.faint = function(target){
		console.log(target.name + " fainted.")
	}
}

let bidoof = new Pokemon ("Bidoof", 100, 300, 500);
let arceus = new Pokemon ("Arceus", 90, 100, 100);
let rowlet = new Pokemon ("Rowlet", 100, 500, 700);

bidoof.tackle(arceus);